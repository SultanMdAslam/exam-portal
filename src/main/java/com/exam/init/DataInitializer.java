package com.exam.init;

import com.exam.model.Role;
import com.exam.model.User;
import com.exam.model.UserRole;
import com.exam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class DataInitializer implements CommandLineRunner {

    @Autowired
    private UserService userService;
    @Override
    public void run(String... args) throws Exception {
        System.out.println("starting code");
        User user = new User();
        user.setFirstName("Sultan");
        user.setLastName("Aslam");
        user.setUsername("aslam1993");
        user.setPassword("abc");
        user.setEmail("smaslam199320@gmail.com");
        user.setProfile("default.png");

        Role role = new Role();
        role.setRoleId(44L);
        role.setRoleName("ADMIN");
        Set<UserRole> userRoles = new HashSet<>();
        UserRole userRole = new UserRole();
        userRole.setRole(role);
        userRole.setUser(user);
        userRoles.add(userRole);
        User user1 = this.userService.createUser(user, userRoles);

        System.out.println(user1.getUsername());
        System.out.println("----------");

    }
}
